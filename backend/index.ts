import express from 'express';
import {DB_connection} from './src/helpers/DBConnection'
import { BlogPostModel } from "./src/models/BlogPostModel";


const app = express();
const PORT:Number=3000;
const cors = require('cors');
const db:DB_connection= new DB_connection();
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(cors({
    origin: '*'
}));

app.use('/', function(req, res, next) {
    
  });


app.listen(PORT,() => {
    console.log('The application is listening '
          + 'on port http://localhost:'+PORT);
})