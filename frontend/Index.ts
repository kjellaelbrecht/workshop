function getdata() {
    
}

function RenderDetails(blogpost:BlogPostModel) {   
    
}

function ShowDetails(id: string) {
    
}

function RenderBlogPost(blogpost: BlogPostModel): void{

}


getdata();


document.getElementById('add').addEventListener('click',() =>{
    var elem = document.getElementById('blog')?.remove();
    document.getElementById('post').style.display = "flex";
    document.getElementById('add').style.display = "none";
})

document.getElementById('postblog').addEventListener('click',() => {
    var name = document.forms['postblogcontent']['name'].value
    var lastname = document.forms['postblogcontent']['lastname'].value
    var title = document.forms['postblogcontent']['title'].value
    var content = document.forms['postblogcontent']['content'].value
    if(name != "" && lastname != "" && title != "" && content !=""){
        var postmodel = new BlogPostModel(0,name,lastname,title,content.toString()).tojson()
        document.forms['postblogcontent']['name'].value ='';
        document.forms['postblogcontent']['lastname'].value ='';
        document.forms['postblogcontent']['title'].value ='';
        document.forms['postblogcontent']['content'].value ='';
        fetch('http://localhost:3000/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(postmodel),
            })
            .then(() => new Promise(resolve => setTimeout(resolve, 1000)))
            .then(() =>{
                document.getElementById('post').style.display = "none";
                document.getElementById('add').style.display = "block";
                getdata();
            })
            .catch((error) => {
            console.error('Error:', error);
            });
    }
    else{
        alert('please fil in the fields')
    }
    
})

document.getElementById('back').addEventListener('click',() =>{
    document.forms['postblogcontent']['name'].value ='';
    document.forms['postblogcontent']['lastname'].value ='';
    document.forms['postblogcontent']['title'].value ='';
    document.forms['postblogcontent']['content'].value ='';
    document.getElementById('post').style.display = "none";
    document.getElementById('add').style.display = "block";
    getdata()
})





class AuthorModel{
    Name: string;
    LastName: string;

    constructor(Name: string, LastName: string){
        this.Name = Name;
        this.LastName = LastName
    }

    toString() :string{
        return this.Name + " " + this.LastName;
    }
}

class BlogPostContentModel{
    Title: string;
    Content: string;

    constructor(Title: string, Content: string){
        this.Title = Title;
        this.Content = Content;
    }
}

class BlogPostModel {
    id: number;
    Author: AuthorModel;
    Content: BlogPostContentModel
    
    constructor(id: number,Name: string, LastName: string, Title: string, Content: string) {
        this.id = id;
        this.Author = new AuthorModel(Name,LastName);
        this.Content = new BlogPostContentModel(Title,Content)
    }
    
    isempty(): boolean{
        if(this.Author.Name == '' && this.Author.LastName == '' && this.Content.Title == '' && this.Content.Content == ''){
            return true
        }
        return false
    }
    tojson():any{
        var ret = {
            "Author": {
                "Name": this.Author.Name,
                "LastName": this.Author.LastName
            },
            "Content": {
                "Title": this.Content.Title,
                "Content": this.Content.Content
            }
        }
        return ret;
    }
}